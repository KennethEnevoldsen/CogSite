---
weight: 7
title: "Guides"
---

# Plotting guides

{{< details title="ggplot2" open=false >}}
A Tutorial for Beautiful Plotting in R

https://cedricscherer.netlify.app/2019/08/05/a-ggplot2-tutorial-for-beautiful-plotting-in-r/
{{< /details >}}

{{< details title="STHDA" open=false >}}
Guides for 'Statistical tools for high-throughput data analysis'

http://www.sthda.com/english/articles/24-ggpubr-publication-ready-plots/
{{< /details >}}
