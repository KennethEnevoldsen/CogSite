---
weight: 7
# bookFlatSection: true
title: "CogTalks"
---

# Cog Productions

## CogTalks

{{< details title="Industry Adventures with Jonathan H. Rystrøm" open=false >}}
{{< youtube unudHAKMC9A >}}

What does a data scientist actually do? Is it all awesome state-of-the-art neural network tuning, mainly powerpoints, or a mix of the two? This CogTalk will let you in on the secret! I will explain what I learned (and didn't learn) from working 1,5 years as a junior data science consultant. It will be a story of awesome models, stupid models, annoying cleaning personnel (what?) and much more. I will go through some (hopefully) interesting cases so you don't have to make all the (many) mistakes I did :))

[Get the slides here](/cogtalks/jonathan-industry.pptx)
{{< /details >}}

{{< details title="Spider Cognition with Arnault-Quentin Vermillet" open=false >}}
{{< youtube xMI8UY5tRiE >}}
{{< /details >}}

{{< details title="Linear Regression with Kenneth Enevoldsen" open=false >}}
{{< youtube pa5uvi0WDpI >}}
{{< /details >}}

{{< details title="Noam Chomsky and Generative Linguistics with Johanne K. Nedergård" open=false >}}
{{< youtube sTC9gugg8h4 >}}
{{< /details >}}

{{< details title="Art Perception with Pernille B. Lassen" open=false >}}
{{< youtube t9uRdeUGF7c >}}
{{< /details >}}

{{< details title="Consciousness with Christoffer L. Olesen" open=false >}}
{{< youtube SeXONznYB0s >}}
{{< /details >}}


{{< details title="Brain-Machine Interfacing with Esben Kran" open=false >}}
{{< youtube nDDIAIdYCoc >}}
[Download slides](../../../cogtalks/brain-machine/slides.pdf)

### Notes

#### References

- <https://neuralink.com>
  - Neuralink, the example for invasive brain-machine interfacing technology used in the talk
- <https://www.braingate.org/>
  - The academic consortium using the Utah Array and developing next-generation academic brain-machine interfaces
- <https://www.kernel.co/hello-humanity>
  - Kernel, the main non-invasive brain-machine interfacing technology used as an example in the talk
- <https://www.extremetech.com/extreme/162678-harvard-creates-brain-to-brain-interface-allows-humans-to-control-other-animals-with-thoughts-alone>
  - Study where a human controlled a rat tail through brain signals, brain-to-brain interface
- <https://www.the-scientist.com/daily-news/a-brain-to-brain-interface-for-rats-39712>
  - Two rats with connected brains

{{< /details >}}


{{< details title="Casual Causality with Jonathan Rystrøm" open=false >}}
{{< youtube xMvI4bLWoSU >}}
{{< /details >}}


{{< details title="Citizen Science with Blanka Palfi" open=false >}}
{{< youtube J6Tw4D3MKYA >}}
{{< /details >}}


{{< details title="Psychedelics with Lasse D. Hansen" open=false >}}
{{< youtube 2qp2Hltyn4w >}}
{{< /details >}}


## CogSeminars
{{< details title="Gambling Agents with Joshua Charles Skewes" open=false >}}
{{< youtube NMsXMLL-CEc >}}
{{< /details >}}